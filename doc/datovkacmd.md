Program DatovkaCmd slouží pro komunikaci se serverem datových schránek. Jeho základní funkčností je:

1. Odeslat zprávy do datové schránky
1. Příjmout zprávy z datové schránky
1. Přesunout přijaté zprávy z databáze tisnet do databáze topeko

## Informace ke spuštění programu ##
Program DatovkaCmd se spouští s parametry **-o -p**, ve smyslu funkčnosti odeslat, přijmout, přenést. Pro spuštění tohoto programu použijte naplánované úlohy Windows. Kontrola datové schránky by se měla provádět v pracovní dobu každou hodinu.

O tom jestli je spuštění a konfigurace řádně provedena se dozvíte z informací, které se zapisují přímo do terminálu. Stejné info však najdete také v souboru **DatovkaCmd.log**, který bude obsahovat informace i v přípdě kdy terminál není spuštěn.

## Informace k instalaci ##
* Program DatovkaCmd je umístěn na downloadu ve složce TISNet2_aktualizace v balíku **TISNet2_2_x_x.zip** 
* Soubory z balíku umístěte do adresáře **c:\topspin\TISNet2**
* Pro úspěšné spuštění je nutné nastavit konfigurační soubory DatovkaCmd.json a tisnet.config
* tisnet.config již není šifrovaný a proto není možné použít původní soubor z adresáře TISNet
* Hodnoty pro **DatovkaCmd.json** je potřeba zjistit přímo z databáze pomocí ManagementStudia, nebo obdobného nástroje
* Program PodatelnaCmd používejte pouze na serverech v adresářích s omezeným přístupem uživatelů, tak aby neměli přístup ke konfiguračním souborům kde je uloženo heslo k databázi

## Informace o nastavení ##

Obsah souboru tisnet.config

```
#!xml

<?xml version="1.0" encoding="utf-8" ?>
<configuration>
  <connectionStrings>
    <clear />
    <add name="tisnet" connectionString="Data Source=.\topspin;Initial Catalog=TISNet;user=sa;password=xxxxxx" providerName="System.Data.SqlClient" />
  </connectionStrings>
</configuration>

```

Obsah souboru DatovkaCmd.json

```
#!json

{
  "PodaciDenikId": "562CC702-011B-43DA-BF1A-1424049E2712",
  "FunkceUzivateleId": "7A1FBD7C-4FB5-4231-9A66-E1B46C48A900",
  "UzivatelId": "EB2DC27D-BBCD-485A-ADA4-0436D96EF98C",
  "WsSadaId": "274EDC2F-C820-4B72-9202-F4A54B76F218" 
}

```

Příkazy pro zajištění identifikátorů v souboru DatovkaCmd.json

```
#!sql

select * from Spisovka.PodaciDenik
select * from Organizace.Uzivatel
select * from Organizace.FunkceUzivatele
select * from ISDS.DsWeboveSluzbySada
```