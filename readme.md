## IS TIS ##

Informační systém TIS

## Obsah 

Obsah dokumentace k Informačnímu systému TIS

## Moduly ##
* [Příjmy]()
* [Pokladna]()
* [Banka]()
* [Párování]()
* [Spisová služba]()

## Správa systému ##

* [Zabezpečení TISu]()
* [DatovkaCmd](https://bitbucket.org/topspincz/tis/src/master/doc/datovkacmd.md)
* [Spuštění TISu]()
* [Zálohování databází]()
* [Saldo]()
* [Párování]()
* [Párování kaskádou]()
* [Roční uzávěrka]()
* [Automatická reinstalace]()
* [Adresářová struktura]()
* [Řídící soubory]()
* [Tiskové sestavy]()