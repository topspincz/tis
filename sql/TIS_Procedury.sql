/* 
* Název:      TIS_Procedury.sql
* Účel:       Software TIS 
* Vlastník:   TOPSPIN Solutions, s.r.o. 
* Autor:      Martin Matousek
* Změna:      13.10.2017
*/

DECLARE @verze_skriptu varchar(11)
SET @verze_skriptu = '01.01.41.23'

/*
* Upozornění: 
* Aby mohl doběhnout v pořádku skript pro založení procedur, je potřeba nejdříve povýšit strukturu databáze. 
* To lze provést samotným spuštěním TISu 
*
* Změny:
* 13.10.2017: Změna názvu skriptu na TIS_Procedury.sql
* 13.10.2017: Doplnění procedury PrenesPredpisyNaZastupce 
* 13.10.2017: Doplnění procedury SjednotZdvojeneUhrady
* 13.10.2017: Doplnění procedury NastavShodnyOpravnyPredpis
* 13.06.2017: Změna funkce StavDokladu, která špatně pro dávku složenek zjišťovala stav spárování na položku bankovního výpisu
*/

USE topeko

IF (dbo.DejCisloVerze(@verze_skriptu) > dbo.DejCisloVerzeDb()) 
	RaisError ('!!! ***  Nejdříve aktualizujte strukturu databáze pomocí spuštění TISu a až poté spusťte tento skript', 20 , -1) with log

UPDATE adsezver SET verdb=@verze_skriptu WHERE modul = 'Procedury'


GO

IF NOT EXISTS (SELECT * FROM adcistag WHERE tag=685) INSERT INTO adcistag (tag, nazev, zkratka, akt, ikona, tpl, typObjektu) VALUES (685, 'Položka interfacu', 'INT', 10, null, 'itpolfps', 20)
GO
IF OBJECT_ID('Poplatky.PrenesPredpisyNaZastupce') IS NOT NULL EXEC('DROP PROCEDURE Poplatky.PrenesPredpisyNaZastupce')
GO
IF OBJECT_ID('Poplatky.DuplikujPredpis') IS NOT NULL EXEC('DROP PROCEDURE Poplatky.DuplikujPredpis')
GO
IF OBJECT_ID('PrenesPredpisyNaZastupce') IS NOT NULL EXEC('DROP PROCEDURE PrenesPredpisyNaZastupce')
GO
IF OBJECT_ID('DuplikujPredpis') IS NOT NULL EXEC('DROP PROCEDURE DuplikujPredpis')
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Uhrady]') AND type in (N'V'))
DROP VIEW [Uhrady]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[OdlisneSouctyUhrad]') AND type in (N'V'))
DROP VIEW [OdlisneSouctyUhrad]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[NastavShodnyOpravnyPredpis]') AND type in (N'P', N'PC'))
DROP PROCEDURE [NastavShodnyOpravnyPredpis]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SjednotZdvojeneUhrady]') AND type in (N'P', N'PC'))
DROP PROCEDURE [SjednotZdvojeneUhrady]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[RepairPar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [RepairPar]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ZnovuNastavDatOrg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [ZnovuNastavDatOrg]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SparujDavkySlozenek]') AND type in (N'P', N'PC'))
DROP PROCEDURE [SparujDavkySlozenek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DuplikujUhradu]') AND type in (N'P', N'PC'))
DROP PROCEDURE [DuplikujUhradu]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DuplikujUhraduNesnizujPuvodni]') AND type in (N'P', N'PC'))
DROP PROCEDURE [DuplikujUhraduNesnizujPuvodni]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[KaskadaNapojeni]') AND type in (N'P', N'PC'))
DROP PROCEDURE [KaskadaNapojeni]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[KaskadaSS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [KaskadaSS]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kaskada]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Kaskada]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[OpacnaKaskadaTable]') AND type in (N'P', N'PC', N'FN'))
DROP PROCEDURE [OpacnaKaskadaTable]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[KaskadaTable]') AND type in (N'P', N'PC', N'FN'))
DROP PROCEDURE [KaskadaTable]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SparujStornoPolozky]') AND type in (N'P', N'PC'))
DROP PROCEDURE [SparujStornoPolozky]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SparujKaskadouVratky]') AND type in (N'P', N'PC'))
DROP PROCEDURE [SparujKaskadouVratky]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SparujKaskadou]') AND type in (N'P', N'PC'))
DROP PROCEDURE [SparujKaskadou]
GO

/* ------ Funkce pro účetní oprávky ----*/ 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DejZobrazeniUcetnichOpravek]') AND type in (N'P', N'PC', N'FN', N'IF', N'TF'))
DROP FUNCTION [DejZobrazeniUcetnichOpravek]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DejZobrazeniPredpisuVSouctu]') AND type in (N'P', N'PC', N'FN', N'IF', N'TF'))
DROP FUNCTION [DejZobrazeniPredpisuVSouctu]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DejZobrazeniPredpisuVSouctuOd]') AND type in (N'P', N'PC', N'FN', N'IF', N'TF'))
DROP FUNCTION [DejZobrazeniPredpisuVSouctuOd]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DejProcentoOpravky]') AND type in (N'P', N'PC', N'FN'))
DROP FUNCTION [DejProcentoOpravky]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SaldoPredpisu]') AND type in (N'P', N'PC', N'FN'))
DROP FUNCTION [SaldoPredpisu]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CheckPar]') AND type in (N'P', N'PC', N'FN'))
DROP FUNCTION [CheckPar]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Parovano]') AND type in (N'P', N'PC', N'FN'))
DROP FUNCTION [Parovano]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DejPosledniSql]') AND type in (N'P', N'PC', N'FN'))
DROP FUNCTION [DejPosledniSql]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SaldoKarty]') AND type in (N'P', N'PC', N'FN'))
DROP FUNCTION [SaldoKarty]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[StavUhrady]') AND type in (N'P', N'PC', N'FN'))
DROP FUNCTION [StavUhrady]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[StavDokladu]') AND type in (N'P', N'PC', N'FN'))
DROP FUNCTION [StavDokladu]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ParovanoTPR]') AND type in (N'P', N'PC', N'FN'))
DROP FUNCTION [ParovanoTPR]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[IsLeap]') AND type in (N'P', N'PC', N'FN'))
DROP FUNCTION [IsLeap]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SparujPodleVS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [SparujPodleVS]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SparujPodleSpojCisla]') AND type in (N'P', N'PC'))
DROP PROCEDURE [SparujPodleSpojCisla]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SparujFakturyPodleVS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [SparujFakturyPodleVS]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SparujNaKartuPodleVS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [SparujNaKartuPodleVS]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SparujPodleSS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [SparujPodleSS]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Sparuj]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Sparuj]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[UlozDoHistorieParovani]') AND type in (N'P', N'PC'))
DROP PROCEDURE [UlozDoHistorieParovani]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SparujNaPredpisVyresZbytek]') AND type in (N'P', N'PC'))
DROP PROCEDURE [SparujNaPredpisVyresZbytek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SparujNaKartuVyresZbytek]') AND type in (N'P', N'PC'))
DROP PROCEDURE [SparujNaKartuVyresZbytek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Odparovat]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Odparovat]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[OdparujPodleSBV]') AND type in (N'P', N'PC'))
DROP PROCEDURE [OdparujPodleSBV]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ZapisDoHistorie]') AND type in (N'P', N'PC'))
DROP PROCEDURE [ZapisDoHistorie]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[ZapisChybu]') AND type in (N'P', N'PC'))
DROP PROCEDURE [ZapisChybu]
GO



/* VIEW ****************************************************************************************/

CREATE VIEW Uhrady AS
SELECT p.*, ISNULL(a.c,p.c) AS Uhrazeno, 'Převod' AS tplpopis FROM posezpar p LEFT OUTER JOIN itpolfps a ON p.idtpl=a.iditpolfps AND p.tpl='itpolfps' WHERE p.tpl='itpolfps'
UNION ALL
SELECT p.*, a.c AS Uhrazeno, 'Banka' AS tplpopis FROM posezpar p INNER JOIN popolbvp a ON p.idtpl=a.idpopolbvp AND p.tpl='popolbvp' WHERE a.akt = 10
UNION ALL
SELECT p.*, a.c AS Uhrazeno, 'Složenky' AS tplpopis FROM posezpar p INNER JOIN popoldav a ON p.idtpl=a.idpopoldav AND p.tpl='popoldav'
UNION ALL
SELECT p.*, a.c AS Uhrazeno, 'Pokladna' AS tplpopis FROM posezpar p INNER JOIN popolpod a ON p.idtpl=a.idpopolpod AND p.tpl='popolpod' WHERE a.akt = 10
UNION ALL
SELECT p.*, a.predpis AS Uhrazeno, 'SIPO' AS tplpopis FROM posezpar p INNER JOIN popolsip a ON p.idtpl=a.idpopolsip AND p.tpl='popolsip'
GO

CREATE VIEW OdlisneSouctyUhrad AS
	SELECT s.idtpl, p.c, p.vs, s.soucet FROM popolbvp p, 
	(
	SELECT SUM(c) AS soucet, idtpl FROM posezpar WHERE akt=10 AND tpl='popolbvp' GROUP BY idtpl
	) s
	WHERE p.idpopolbvp = s.idtpl and p.c<>s.soucet
	UNION ALL
	SELECT s.idtpl, p.c, p.vs, s.soucet FROM popolpod p, 
	(
	SELECT SUM(c) AS soucet, idtpl FROM posezpar WHERE akt=10 AND TPL='popolpod' GROUP BY idtpl
	) s
	WHERE p.idpopolpod = s.idtpl AND ABS(p.c)<>ABS(s.soucet)
	UNION ALL
	SELECT s.idtpl, p.c, p.vs, s.soucet FROM popoldav p, 
	(
	SELECT SUM(c) AS soucet, idtpl FROM posezpar WHERE akt=10 AND TPL='popoldav' GROUP BY idtpl
	) s
	WHERE p.idpopoldav = s.idtpl AND p.c<>s.soucet
	GO



/* FUNKCE ****************************************************************************************/

/* Funkce CheckPar provádí kontrolu součtu částek v tabulce párování a zdrojové tabulce úhrad. Součet by měl být shodný. Funkce se používá u constraintu CK_CheckPar */
CREATE FUNCTION CheckPar(@tpl varchar(10), @idtpl integer) RETURNS numeric(12,2) as
BEGIN
  DECLARE @par_c numeric(12,2)
  DECLARE @source_c numeric(12,2)
  DECLARE @tpi smallint

  SELECT @par_c = sum(c) FROM posezpar WHERE tpl=@tpl and idtpl=@idtpl and akt=10

  IF (@tpl = 'popolbvp') 
    SELECT @source_c = c FROM popolbvp WHERE idpopolbvp = @idtpl and akt=10

  IF (@tpl = 'popolpod') 
  BEGIN
    SELECT @source_c = c, @tpi = tpi FROM posezpod s, popolpod p WHERE s.idposezpod = p.idposezpod AND idpopolpod = @idtpl and p.akt=10
    IF (@tpi = 320) SET @source_c = - (@source_c)
  END

  IF (@tpl = 'popoldav') 
    SELECT @source_c = c FROM popoldav WHERE idpopoldav = @idtpl


  RETURN ISNULL(@par_c,0) - ISNULL(@source_c,0)
END
GO

/* ------ Funkce pro kaskádu ----*/
CREATE FUNCTION DejPosledniSql(@sid int) RETURNS nvarchar(max) AS
BEGIN
  DECLARE @sqlstr nvarchar(max)
  DECLARE @HANDLE BINARY(20)
  
  SELECT @HANDLE = sql_handle from sys.sysprocesses where spid = 60
  SELECT @sqlstr = text FROM ::fn_get_sql(@handle)
    
  RETURN @sqlstr
END
GO

CREATE FUNCTION dbo.IsLeap(@year SMALLINT) RETURNS bit AS
BEGIN
   RETURN (SELECT CASE WHEN ((@year % 4 = 0 AND @year % 100 <> 0) OR (@year % 400 = 0)) THEN 1 ELSE 0 END)
END
GO

CREATE FUNCTION SaldoKarty(@idaasezfps integer) RETURNS numeric(12,2) AS
BEGIN
  DECLARE @saldo numeric(12,2)
  
  SELECT @saldo=sum(ISNULL(c,0)) FROM 
  (
    SELECT ISNULL(pocstav,0) as c FROM aasezfps WHERE idaasezfps=@idaasezfps AND akt in (10, 20, 30)
    UNION ALL
      SELECT ISNULL(c,0) FROM aapolfps WHERE idaasezfps=@idaasezfps AND akt=10
    UNION ALL
      SELECT ISNULL(-c,0) FROM posezpar WHERE idaasezfps=@idaasezfps AND akt=10
  ) spojeno

  RETURN ISNULL(@saldo,0)
END
GO

CREATE FUNCTION Parovano(@idaapolfps integer) RETURNS numeric(12,2) AS
BEGIN
  DECLARE @celkem numeric(12,2)
  SELECT @celkem = sum(ISNULL(c,0)) FROM posezpar WHERE idaapolfps = @idaapolfps AND akt=10
  RETURN ISNULL(@celkem,0)
END
GO


CREATE FUNCTION SaldoPredpisu(@idaasezfps integer, @datspl datetime) RETURNS numeric(12,2) AS
BEGIN
  DECLARE @celkem numeric(12,2)
  DECLARE @predpisy Predpisy_TableType 

  INSERT INTO @predpisy 
  SELECT p.idaapolfps, p.idaasezfps, p.c, dbo.Parovano(idaapolfps), p.datspl FROM aapolfps p WHERE idaasezfps=@idaasezfps AND datspl=@datspl AND akt = 10 

  SELECT @celkem = SUM(c-cpar) FROM @predpisy

  RETURN ISNULL(@celkem,0)
END
GO


CREATE FUNCTION ParovanoTPR(@tpr varchar(10), @idtpr integer) RETURNS numeric(12,2) AS
BEGIN
  DECLARE @celkem numeric(12,2)
  SELECT @celkem = sum(ISNULL(c,0)) FROM posezpar WHERE tpr=@tpr AND idtpr = @idtpr AND akt=10
  RETURN ISNULL(@celkem,0)
END
GO

CREATE FUNCTION StavUhrady(@idtpl int, @tpl varchar(10)) RETURNS varchar(3) AS
BEGIN
  DECLARE @sbv varchar(3)

  SELECT @sbv = sbv FROM posezpar WHERE idtpl = @idtpl AND tpl = @tpl and akt=10
  IF @sbv IS NULL 
  BEGIN
    IF @tpl = 'popolbvp' SELECT @sbv = sbv FROM popolbvp WHERE idpopolbvp = @idtpl
	IF @tpl = 'popoldav' SELECT @sbv = sbv FROM popoldav WHERE idpopoldav = @idtpl
	IF @tpl = 'popolpod' SELECT @sbv = sbv FROM popolpod WHERE idpopolpod = @idtpl
  END

  RETURN @sbv
END
GO

CREATE FUNCTION [dbo].[StavDokladu](@id int, @doklad varchar(10)) RETURNS varchar(3) AS
BEGIN
  DECLARE @sbv varchar(3)

  IF @doklad = 'posezbvp' SELECT @sbv = sbv FROM popolbvp WHERE idposezbvp = @id AND dbo.StavUhrady(idpopolbvp, 'popolbvp') = 'NES'
  IF @doklad = 'posezdav' 
  BEGIN
	SELECT @sbv = sbv FROM posezpar WHERE idtpr = @id AND tpr = 'posezdav' and akt=10
	IF @sbv IS NULL 
	  SELECT @sbv = sbv FROM posezdav WHERE idposezdav = @id
  END
  IF @doklad = 'posezpod' SELECT @sbv = sbv FROM popolpod WHERE idpopolpod = @id AND dbo.StavUhrady(idpopolpod, 'popolpod') = 'NES'

  IF @sbv IS NULL 
	SET @sbv = 'SPA'

  RETURN @sbv
END
GO

/* ------ Funkce pro účetní oprávky ----*/
CREATE FUNCTION DejProcentoOpravky(@StariDluhu int) RETURNS int AS
BEGIN
  RETURN CASE WHEN ((@StariDluhu/90)>10) THEN 100 ELSE (Floor(@StariDluhu / 90) * 10) END
END
GO


CREATE FUNCTION DejZobrazeniPredpisuVSouctu(@do datetime) RETURNS TABLE AS
    RETURN (
    select *, dbo.SaldoPredpisu(idaasezfps, datspl) as SaldoPredpisu FROM 
	( select f.tfp, p.idaasezfps, min(idaapolfps) as idaapolfps, p.datspl from aasezfps f LEFT JOIN aapolfps p ON f.idaasezfps=p.idaasezfps WHERE p.datspl <= @do AND f.akt IN (10,20,30) AND p.akt=10 GROUP BY f.tfp, p.idaasezfps, p.datspl ) HlavniPredpisy
	)
GO

CREATE FUNCTION [dbo].[DejZobrazeniPredpisuVSouctuOd](@do datetime, @od datetime) RETURNS TABLE
AS
RETURN SELECT tfp, idaasezfps, idaapolfps, datspl, dbo.SaldoPredpisu(idaasezfps, datspl) as SaldoPredpisu FROM 
	    ( select f.tfp, p.idaasezfps, min(idaapolfps) as idaapolfps, p.datspl from aasezfps f LEFT JOIN aapolfps p ON f.idaasezfps=p.idaasezfps WHERE p.datspl >= @od AND p.datspl <= @do AND f.akt IN (10,20,30) AND p.akt=10 GROUP BY f.tfp, p.idaasezfps, p.datspl ) HlavniPredpisy
GO

CREATE FUNCTION DejZobrazeniUcetnichOpravek (@DatumVypoctu datetime, @od datetime = null) RETURNS TABLE AS
  RETURN 
  (
    SELECT *, DATEDIFF(dd, datspl,@DatumVypoctu) AS StariDluhu, dbo.DejProcentoOpravky(DATEDIFF(dd, datspl,@DatumVypoctu)) AS ProcentoOpravky, (dbo.DejProcentoOpravky(DATEDIFF(dd, datspl,@DatumVypoctu)) * SaldoPredpisu / 100) AS VyseOpravky FROM DejZobrazeniPredpisuVSouctuOd(@DatumVypoctu, @od) WHERE SaldoPredpisu > 0
  )
GO


/* PROCEDURY *************************************************************************************/
/* */
/* */
/* */
/* */

CREATE PROCEDURE DuplikujPredpis(@idaapolfps int, @idaasezfps int, @popis nvarchar(100), @c numeric(15,2)) AS
BEGIN
  DECLARE @newidpol int

  SELECT @newidpol = MAX(idaapolfps) + 1 FROM aapolfps
  INSERT INTO aapolfps (idaapolfps, id, idaasezfps, idaaseztsk, idaasezktp, ideksezcph, datspl, datvzn, tag, tpi, tpitxt, prk, dph, cdph, popis, vs, ss, jednotkajesdph, pj, mj, cmj, c, datumvytvoreni, datumzmeny, akt, svp, datsvp, from_idaasezfps) 
	SELECT @newidpol, id, @idaasezfps, idaaseztsk, idaasezktp, ideksezcph, datspl, datvzn, tag, tpi, tpitxt, prk, dph, cdph, @popis+'. '+ISNULL(popis,''), vs, ss, jednotkajesdph, 1, mj, @c, @c, GETDATE(), GETDATE(), akt, svp, datsvp, @idaapolfps FROM aapolfps WHERE idaapolfps = @idaapolfps						
END
GO

CREATE PROCEDURE PrenesPredpisyNaZastupce(@idaasezfps int, @idadsezusr int, @testRun int, @runGuid uniqueidentifier, @dueDate datetime, @fromDate datetime) AS
BEGIN
	DECLARE @to_idaasezfps int
	DECLARE @to_idaasezfps2 int
	DECLARE @from_idaasezfps int
	DECLARE @from_objmeno nvarchar(100)
	DECLARE @to_objmeno nvarchar(100)
	DECLARE @idzzastsub int
	DECLARE @idzzastsub2 int
	DECLARE @newidpol int
	DECLARE @c numeric(15,2)
	--DECLARE @cmj numeric(15,2)
	DECLARE @saldo numeric(15,2)
	DECLARE @tfp int	
	DECLARE @idaapolfps int
	
	DECLARE predpisy CURSOR FOR	
	SELECT p.idaapolfps, s.idzzastsub, s.idzzastsub2, f.idaasezfps, s.objmeno, p.c, dbo.SaldoPredpisu(p.idaasezfps, p.datspl), f.tfp
	FROM aapolfps p LEFT JOIN aasezfps f ON p.idaasezfps = f.idaasezfps LEFT JOIN aasezsub s ON f.idaasezsub = s.idaasezsub 
	WHERE dbo.SaldoPredpisu(p.idaasezfps, p.datspl) > 0 and p.datspl >= @fromDate and p.datspl <= @dueDate and p.c > 0 and p.akt = 10 and 
	ISNULL(s.idzzastsub,0) > 0 and f.idaasezfps = @idaasezfps

	OPEN predpisy
	FETCH NEXT FROM predpisy INTO @idaapolfps, @idzzastsub, @idzzastsub2, @from_idaasezfps, @from_objmeno, @c, @saldo, @tfp
	WHILE @@FETCH_STATUS = 0   
	BEGIN 
		SELECT @to_idaasezfps = idaasezfps FROM aasezfps f LEFT JOIN aasezsub s ON f.idaasezsub = s.idaasezsub WHERE f.tfp = @tfp AND s.idaasezsub = @idzzastsub AND f.akt = 10 
		SELECT @to_objmeno = s.objmeno FROM aasezfps f LEFT JOIN aasezsub s ON f.idaasezsub = s.idaasezsub WHERE f.tfp = @tfp AND f.idaasezfps = @to_idaasezfps AND f.akt = 10 

		IF (@to_idaasezfps IS NOT NULL) 
		BEGIN
			SET @from_objmeno = Cast('Převod dluhu od ' as nvarchar(100)) + @from_objmeno
			EXEC DuplikujPredpis @idaapolfps, @to_idaasezfps, @popis = @from_objmeno, @c = @saldo

			SET @to_objmeno = Cast('Dluh přenesen na ' as nvarchar(100)) + @to_objmeno
			SET @saldo = -@saldo
			EXEC DuplikujPredpis @idaapolfps, @from_idaasezfps, @to_objmeno, @c = @saldo

			INSERT INTO aalogfps (idaapolfps_from, idaapolfps_to, idadsezusr) VALUES (@from_idaasezfps, @to_idaasezfps, @idadsezusr)
		END
	   FETCH NEXT FROM predpisy INTO @idaapolfps, @idzzastsub, @idzzastsub2, @from_idaasezfps, @from_objmeno, @c, @saldo, @tfp
	END

	CLOSE predpisy
	DEALLOCATE predpisy
END
GO

/* Procedurea RepairPar vyhledá chybové záznamy v tabulce párování, odstraní je a u zdrojové tabulky nastaví příznak na pořízeno. Při spuštění párování se tyto záznamy znovu doplní do tabulky párování */
CREATE PROCEDURE RepairPar(@tpl varchar(10)) AS
BEGIN
  DECLARE @idtpl int
  DECLARE platby CURSOR FOR  
  SELECT idpopolbvp from popolbvp where akt=10 and dbo.CheckPar(@tpl, idpopolbvp)<>0
  OPEN platby
  FETCH NEXT FROM platby INTO @idtpl

  WHILE @@FETCH_STATUS = 0   
  BEGIN 
    UPDATE posezpar SET akt=80 WHERE idtpl=@idtpl and tpl=@tpl and akt=10
	IF (@tpl = 'popolbvp') UPDATE popolbvp SET sbv='POR' WHERE idpopolbvp = @idtpl
	IF (@tpl = 'popoldav') UPDATE popoldav SET sbv='POR' WHERE idpopoldav = @idtpl
	IF (@tpl = 'popolpod') UPDATE popolpod SET sbv='POR' WHERE idpopolpod = @idtpl

    FETCH NEXT FROM platby INTO @idtpl
  END

  CLOSE platby
  DEALLOCATE platby
END
GO


CREATE Procedure UlozDoHistorieParovani(@idposezpar int, @tpr varchar(10), @idtpr int, @idadsezusr int, @sbv varchar(3), @popis nvarchar(255)) AS
BEGIN
  INSERT INTO pologpar (idposezpar, tpr, idtpr, idadsezusr, sbv, popis) VALUES (@idposezpar, @tpr, @idtpr, @idadsezusr, @sbv, @popis)
END
GO


CREATE PROCEDURE ZapisChybu(@idadsezusr int, @popis nvarchar(255)) AS
BEGIN
   INSERT INTO adsezerr SELECT
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage,
		dbo.DejPosledniSql(@@SPID),
		GetDate() as TimeStamp,		
		@idadsezusr,
		@popis

   PRINT 'CHYBA '+@popis+' '+ERROR_MESSAGE()

   RETURN SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;
   
END
GO


CREATE PROCEDURE OdparujPodleSBV(@sbv varchar(3), @idposezpar integer, @tpl varchar(50), @idtpl integer, @popis nvarchar(255)) AS
BEGIN
  DECLARE @sql varchar(255)

  IF @sbv = 'SPA'
	UPDATE posezpar SET datumzmeny=GETDATE(), sbv=@sbv, idaapolfps = null, popis = @popis where idposezpar=@idposezpar   

  IF (@sbv = 'NES') or (@sbv = 'ODL') 
    UPDATE posezpar SET datumzmeny=GETDATE(), sbv=@sbv, idaapolfps = null, idaasezfps = null, popis = @popis where idposezpar=@idposezpar

  IF @tpl <> 'itpolfps' 
  BEGIN
    SET @sql = 'UPDATE ' + @tpl + ' SET sbv = ' + @sbv + ' WHERE id' + @tpl + ' = ' + @idtpl
    EXEC @sql
  END
END
GO

CREATE PROCEDURE Sparuj(@sbv varchar(3), @idaasezfps int, @idaapolfps int, @idposezpar int, @idadsezusr int, @popis nvarchar(255)) AS
BEGIN
  IF @idaapolfps IS NULL
  BEGIN
    IF @sbv <> 'SPM' SET @sbv='SPA'
    UPDATE posezpar SET sbv=@sbv, idaasezfps = @idaasezfps, idadsezusr=@idadsezusr, popis=@popis WHERE idposezpar = @idposezpar
  END ELSE
  BEGIN
    IF @sbv <> 'SPM' SET @sbv='SPK'
    UPDATE posezpar SET sbv=@sbv, idaapolfps = @idaapolfps, idaasezfps = @idaasezfps, idadsezusr=@idadsezusr, popis=@popis WHERE idposezpar = @idposezpar
  END 
END
GO

CREATE PROCEDURE DuplikujUhradu(@stary_id integer, @nova_platba_c numeric(12,2), @novy_sbv varchar(3), @novy_id integer OUTPUT, @idadsezusr int) AS
BEGIN
  DECLARE @idaasezfps int
  DECLARE @idaapolfps int
  DECLARE @stary_sbv varchar(3)

  -- zajisti novy identifikator
  SELECT @novy_id = MAX(idposezpar)+1 FROM posezpar 
  SELECT @idaasezfps = idaasezfps, @idaapolfps = idaapolfps, @stary_sbv = sbv FROM posezpar WHERE idposezpar = @stary_id
  --SELECT @novy_id = SCOPE_IDENTITY() 
  
  -- duplikuj uhradu
  INSERT INTO posezpar (idposezpar, sbv, tpi, idaasezfps, idtpl, tpl, datzap, datorg, c, vs, ss, ks, spojcislo, ucetcislo_ci, ucetkod_ci, akt, tpitxt, popis, uctdok, idposezpod, tpr, idtpr, ideksezcph, idaapolfps, datumvytvoreni, datumzmeny, cislo, idadsezusr) 
  SELECT @novy_id, @novy_sbv, tpi, idaasezfps, idtpl, tpl, datzap, datorg, @nova_platba_c, vs, ss, ks, spojcislo, ucetcislo_ci, ucetkod_ci, akt, tpitxt, 'Zdvojení úhrady', uctdok, idposezpod, tpr, idtpr, ideksezcph, NULL, GETDATE(), GETDATE(), cislo, @idadsezusr FROM posezpar WHERE idposezpar = @stary_id  

  -- zmensi puvodni uhradu
  UPDATE posezpar set c=c-@nova_platba_c, idadsezusr=@idadsezusr, popis='Snížení úhrady po zdvojení' WHERE idposezpar=@stary_id  
END;
GO

CREATE PROCEDURE DuplikujUhraduNesnizujPuvodni(@stary_id integer, @nova_platba_c numeric(12,2), @novy_sbv varchar(3), @novy_id integer OUTPUT, @idadsezusr int) AS
BEGIN
  DECLARE @idaasezfps int
  DECLARE @idaapolfps int
  DECLARE @stary_sbv varchar(3)

  -- zajisti novy identifikator
  SELECT @novy_id = MAX(idposezpar)+1 FROM posezpar 
  
  --SELECT @novy_id = SCOPE_IDENTITY() 
  
  -- duplikuj uhradu
  IF @novy_sbv = 'NES'
    INSERT INTO posezpar (idposezpar, sbv, tpi, idaasezfps, idtpl, tpl, datzap, datorg, c, vs, ss, ks, spojcislo, ucetcislo_ci, ucetkod_ci, akt, tpitxt, popis, uctdok, idposezpod, tpr, idtpr, ideksezcph, idaapolfps, datumvytvoreni, datumzmeny, cislo, idadsezusr) 
    SELECT @novy_id, @novy_sbv, tpi, NULL, idtpl, tpl, datzap, datorg, @nova_platba_c, vs, ss, ks, spojcislo, ucetcislo_ci, ucetkod_ci, akt, tpitxt, 'Založení úhrady po zdvojení', uctdok, idposezpod, tpr, idtpr, ideksezcph, NULL, GETDATE(), GETDATE(), cislo, @idadsezusr FROM posezpar WHERE idposezpar = @stary_id

  IF (@novy_sbv = 'SPA') OR (@novy_sbv = 'SBK')
    INSERT INTO posezpar (idposezpar, sbv, tpi, idaasezfps, idtpl, tpl, datzap, datorg, c, vs, ss, ks, spojcislo, ucetcislo_ci, ucetkod_ci, akt, tpitxt, popis, uctdok, idposezpod, tpr, idtpr, ideksezcph, idaapolfps, datumvytvoreni, datumzmeny, cislo, idadsezusr) 
    SELECT @novy_id, @novy_sbv, tpi, idaasezfps, idtpl, tpl, datzap, datorg, @nova_platba_c, vs, ss, ks, spojcislo, ucetcislo_ci, ucetkod_ci, akt, tpitxt, 'Založení úhrady po zdvojení', uctdok, idposezpod, tpr, idtpr, ideksezcph, NULL, GETDATE(), GETDATE(), cislo, @idadsezusr FROM posezpar WHERE idposezpar = @stary_id  

END;
GO


CREATE PROCEDURE SparujNaKartuVyresZbytek(@idaasezfps int, @sbv varchar(3), @idposezpar int OUTPUT, @platba_c numeric(12,2) OUTPUT, @chceme_naparovat numeric(12,2), @idadsezusr int, @popis nvarchar(255)) AS
BEGIN
  DECLARE @novy_idposezpar int
  DECLARE @zbytek numeric(12,2) 

  IF @platba_c >= @chceme_naparovat 
  BEGIN
    
    SET @zbytek = @platba_c - @chceme_naparovat
  
    UPDATE posezpar SET sbv=@sbv, c=@chceme_naparovat ,idaasezfps = @idaasezfps,idadsezusr=@idadsezusr,popis=@popis WHERE idposezpar = @idposezpar

    IF @zbytek <> 0 
    BEGIN
      EXEC dbo.DuplikujUhraduNesnizujPuvodni @idposezpar, @zbytek, 'NES', @novy_idposezpar OUTPUT, @idadsezusr
    END

  END
END
GO


CREATE PROCEDURE SparujNaPredpisVyresZbytek(@idaasezfps int, @idaapolfps int, @sbv varchar(3), @idposezpar int OUTPUT, @platba_c numeric(12,2) OUTPUT, @potrebujem numeric(12,2), @idadsezusr int, @popis nvarchar(255)) AS
BEGIN
  DECLARE @novy_idposezpar int
  DECLARE @zbytek numeric(12,2)
   
    --!!! POVOLUJEM MANUALNE PAROVAT NA VYROVNANY PREDPIS IF (@potrebujem = 0) RETURN -1 --na predpis se da parovat pouze pokud je nevyrovnany

	--UPDATE posezpar SET idaapolfps = @idaapolfps, idaasezfps = @idaasezfps, sbv='SPK' WHERE idposezpar=@idposezpar
	EXEC Sparuj @sbv, @idaasezfps, @idaapolfps, @idposezpar, @idadsezusr, @popis

	-- kolik potrebujem = (@c-@cpar)
	-- kolik mame = @platba_c
	-- zbytek je rozdil toho co mame v platbe a toho co bychom potrebovali
	--  100       200         200 - 100  - jeste zbylo na dalsi predpis
	--  -100      100         200 - 0    - chybi i na tento predpis
	--  0         200         200 - 0    - mame tak akorat 

	-- jestli nam chybi, nebo vyslo presne bude postup stejny
	SET @zbytek = @platba_c - @potrebujem

	IF (@platba_c > 0)  
	BEGIN
		IF (@platba_c >= @potrebujem) AND (@potrebujem <> 0) --v pripade ze parujem rucne, muzem parovat i na vyrovnany predpis a v tom pripade parujem veskerou uhradu  
		BEGIN
		  -- mame dost
		  -- ! nemusime delat, budem resit jenom vypocet UPDATE aapolfps SET cpar=c WHERE idaapolfps = @idaapolfps
		  SET @platba_c = @platba_c - @potrebujem

		  IF @platba_c > 0 
		  BEGIN	  
			EXEC dbo.DuplikujUhradu @idposezpar, @platba_c, @sbv, @novy_idposezpar OUTPUT, @idadsezusr
			SET @idposezpar = @novy_idposezpar
			--EXEC dbo.Kaskada @idaasezfps, @novy_idposezpar rekurze?
		  END
		END ELSE BEGIN
		  -- mame malo
		  -- ! nemusime delat budem resit jenom vypocet UPDATE aapolfps SET cpar=ISNULL(cpar,0)+@platba_c WHERE idaapolfps=@idaapolfps
		  SET @platba_c = 0
		END
    END

	IF (@platba_c < 0) --vydaj z pohledu organizace
	BEGIN
	  IF (@platba_c <= @potrebujem) AND (@potrebujem <> 0) --v pripade ze parujem rucne, muzem parovat i na vyrovnany predpis a v tom pripade parujem veskerou uhradu
	  BEGIN
  	    -- mame dost
		-- ! nemusime delat, budem resit jenom vypocet UPDATE aapolfps SET cpar=c WHERE idaapolfps = @idaapolfps
		SET @platba_c = @platba_c - @potrebujem

		IF @platba_c < 0 
		BEGIN	  
		  EXEC dbo.DuplikujUhradu @idposezpar, @platba_c, @sbv, @novy_idposezpar OUTPUT, @idadsezusr
		  SET @idposezpar = @novy_idposezpar
			--EXEC dbo.Kaskada @idaasezfps, @novy_idposezpar rekurze?
		END
	  END ELSE BEGIN
		  -- mame malo
		  -- ! nemusime delat budem resit jenom vypocet UPDATE aapolfps SET cpar=ISNULL(cpar,0)+@platba_c WHERE idaapolfps=@idaapolfps
		  SET @platba_c = 0
	  END
	END
END
GO

CREATE PROCEDURE OpacnaKaskadaTable (@predpisy Predpisy_TableType ReadOnly, @idposezpar integer, @platba_c numeric(12,2), @platba_datzap datetime, @idadsezusr int, @popis nvarchar(255))
AS
BEGIN
  DECLARE @Index INT
  DECLARE @RecordCnt INT
  
  DECLARE @idaasezfps integer
  DECLARE @idaapolfps integer
  DECLARE @c numeric(12,2)
  DECLARE @cpar numeric(12,2)
  DECLARE @datspl datetime
  
  DECLARE @novy_idposezpar integer
  DECLARE @zbytek numeric(12,2) 

  SELECT @RecordCnt = COUNT(idaapolfps) FROM @predpisy
  SELECT @Index = 1

  IF (@RecordCnt > 0)
  BEGIN

  WHILE (@Index <= @RecordCnt)
  BEGIN 
    SELECT @idaapolfps=idaapolfps, @idaasezfps = idaasezfps, @c = c, @cpar = ISNULL(cpar,0), @datspl = datspl FROM @predpisy WHERE iSNo=@Index   

    EXEC Sparuj 'SPK',@idaasezfps, @idaapolfps, @idposezpar, @idadsezusr, @popis	  

	IF @platba_c <= @cpar 
	BEGIN     
	  SET @platba_c = 0
	END ELSE
	BEGIN
	  SET @platba_c = @platba_c - @cpar
	  EXEC dbo.DuplikujUhradu @idposezpar, @platba_c, 'SPA', @novy_idposezpar OUTPUT, @idadsezusr
	END

    SELECT @Index = @Index + 1

	IF @platba_c = 0 
	  BREAK
  END

  END
END
GO


CREATE PROCEDURE KaskadaTable (@predpisy Predpisy_TableType ReadOnly, @idposezpar integer, @platba_c numeric(12,2), @platba_datzap datetime, @idadsezusr int, @popis nvarchar(255))
AS
BEGIN
  DECLARE @Index INT
  DECLARE @RecordCnt INT
  
  DECLARE @idaasezfps integer
  DECLARE @idaapolfps integer
  DECLARE @c numeric(12,2)
  DECLARE @cpar numeric(12,2)
  DECLARE @datspl datetime
  
  DECLARE @novy_idposezpar integer
  DECLARE @zbytek numeric(12,2)
  DECLARE @potrebujem numeric(12,2)

  SELECT @RecordCnt = COUNT(idaapolfps) FROM @predpisy
  SELECT @Index = 1

  IF (@RecordCnt > 0)
  BEGIN

  WHILE (@Index <= @RecordCnt)
  BEGIN 
    SELECT @idaapolfps=idaapolfps, @idaasezfps = idaasezfps, @c = c, @cpar = ISNULL(cpar,0), @datspl = datspl FROM @predpisy WHERE iSNo=@Index
    SET @potrebujem = @c - @cpar

	-- Výdaje se párujou pouze na mínusové předpisy
	-- Příjmy se párujou pouze na kladné předpisy
	IF (@platba_c < 0 AND @c < 0) OR (@platba_c > 0 AND @c > 0)
	  EXEC SparujNaPredpisVyresZbytek @idaasezfps, @idaapolfps, 'SPA', @idposezpar OUTPUT, @platba_c OUTPUT, @potrebujem, @idadsezusr, @popis 

    SELECT @Index = @Index + 1

	IF @platba_c = 0 
	  BREAK
  END

  /* Ošetření vratek

  Sem dojel s vratkou když prošel všecky předpisy, pokud našel nějaký opravný mínusový tak na něj napároval, ale když ne tzn., 
  že platba_c je nenulová, resp. záporná tak zkusíme spustit opačnou kaskádu.

  */

    IF @platba_c < 0 
    BEGIN
      DECLARE @predpisyOpacne Predpisy_TableType

--Opačná kaskáda
  
      INSERT INTO @predpisyOpacne 
        SELECT p.idaapolfps, p.idaasezfps, p.c, dbo.Parovano(idaapolfps), p.datspl FROM aapolfps p JOIN aasezktp k ON p.idaasezktp=k.idaasezktp
        WHERE p.idaasezfps = @idaasezfps and p.c>0 and dbo.Parovano(idaapolfps)>0 and p.akt=10
        ORDER BY p.datspl DESC

      EXEC OpacnaKaskadaTable @predpisyOpacne, @idposezpar, @platba_c, @platba_datzap, @idadsezusr, @popis
    END

  END ELSE
  BEGIN
  /* pokud nenasel vhodne predpisy k naparovani, co dal? moznosti:
       - nechame platbu pouze na karte
	   - sparujeme na posledni predpis
	   - nechame v nesparovanych

	   - ted se nechava tak jak to bylo, tedy na karte
  */
    SET @platba_c = 0
  END
END
GO

CREATE PROCEDURE Kaskada (@idaasezfps integer, @idposezpar integer, @platba_c numeric(12,2), @platba_datzap datetime, @idadsezusr int, @popis nvarchar(255))
AS
BEGIN
  DECLARE @predpisy Predpisy_TableType

  IF @platba_c > 0 
  BEGIN
    INSERT INTO @predpisy 
    SELECT p.idaapolfps, p.idaasezfps, p.c, p.parovano, p.datspl FROM (SELECT MIN(idaapolfps) as idaapolfps, idaasezfps, SUM(c) as c, SUM(dbo.Parovano(idaapolfps)) as parovano, idaasezktp, datspl, datsvp FROM aapolfps WHERE akt=10 GROUP BY idaasezfps, idaasezktp, datspl, datsvp) p JOIN aasezktp k ON p.idaasezktp=k.idaasezktp
    WHERE p.idaasezfps = @idaasezfps and p.c<>0 and p.c<>p.parovano and (p.datsvp is null or @platba_datzap<=p.datsvp)
    ORDER BY k.pripar, p.datspl, k.priparpo
  END ELSE
  BEGIN
    -- pokud jde o vratku tak nebudu scitat predpisy podle datumu splatnosti
    INSERT INTO @predpisy 
    SELECT p.idaapolfps, p.idaasezfps, p.c, dbo.Parovano(idaapolfps), p.datspl FROM aapolfps p JOIN aasezktp k ON p.idaasezktp=k.idaasezktp
    WHERE p.idaasezfps = @idaasezfps AND p.akt=10 AND p.c<>0 AND p.c<>dbo.Parovano(idaapolfps) AND (p.datsvp is null or @platba_datzap<=p.datsvp)
    ORDER BY k.pripar, p.datspl, k.priparpo
  END

  EXEC KaskadaTable @predpisy, @idposezpar, @platba_c, @platba_datzap, @idadsezusr, @popis
  
END
GO

CREATE PROCEDURE KaskadaSS (@vs varchar(10), @ss varchar(10), @idposezpar integer, @platba_c numeric(12,2), @platba_datzap datetime, @idadsezusr int, @popis nvarchar(255))
AS
BEGIN
  DECLARE @predpisy Predpisy_TableType

-- nejdrive predpisy toho kdo skutecne platil, resp. ty se stejnym vs
  INSERT INTO @predpisy 
  SELECT p.idaapolfps, p.idaasezfps, p.c, dbo.Parovano(p.idaapolfps), p.datspl FROM aasezfps f JOIN aapolfps p ON f.idaasezfps=p.idaasezfps JOIN aasezktp k ON p.idaasezktp=k.idaasezktp
  WHERE p.ss = @ss and p.c<>0 and c<>dbo.Parovano(p.idaapolfps) and p.akt=10 and (p.datsvp is null or @platba_datzap<=p.datsvp)
  AND f.vs=@vs AND (LTrim(ISNULL(p.ss,'')) <> '' AND ISNULL(p.ss,'') <> '0000000000')
  ORDER BY k.pripar, p.datspl, k.priparpo

-- a potom predpisy toho kdo neplatil, resp. ty s odlisnym vs
  INSERT INTO @predpisy 
  SELECT p.idaapolfps, p.idaasezfps, p.c, dbo.Parovano(p.idaapolfps), p.datspl FROM aasezfps f JOIN aapolfps p ON f.idaasezfps=p.idaasezfps JOIN aasezktp k ON p.idaasezktp=k.idaasezktp
  WHERE p.ss = @ss and p.c<>0 and c<>dbo.Parovano(p.idaapolfps) and p.akt=10 and (p.datsvp is null or @platba_datzap<=p.datsvp)
  AND f.vs<>@vs AND (LTrim(ISNULL(p.ss,'')) <> '' AND ISNULL(p.ss,'') <> '0000000000')
  ORDER BY k.pripar, p.datspl, k.priparpo

  EXEC KaskadaTable @predpisy, @idposezpar, @platba_c, @platba_datzap, @idadsezusr, @popis
  
END
GO

CREATE PROCEDURE KaskadaNapojeni (@idaasezfps integer, @idposezpar integer, @platba_c numeric(12,2), @platba_datzap datetime, @idadsezusr int, @popis nvarchar(255))
AS
BEGIN
  DECLARE @predpisy Predpisy_TableType

  INSERT INTO @predpisy 
  SELECT p.idaapolfps, p.idaasezfps, p.c, p.parovano, p.datspl FROM aasezfps f JOIN (SELECT MIN(idaapolfps) as idaapolfps, idaasezfps, SUM(c) as c, SUM(dbo.Parovano(idaapolfps)) as parovano, idaasezktp, datspl, datsvp FROM aapolfps WHERE akt=10 GROUP BY idaasezfps, idaasezktp, datspl, datsvp) p ON f.idaasezfps=p.idaasezfps JOIN aasezktp k ON p.idaasezktp=k.idaasezktp
  WHERE (f.idaasezfps = @idaasezfps OR f.idspolfps=@idaasezfps) AND p.c<>0 and p.c<>p.parovano and (p.datsvp is null or @platba_datzap<=p.datsvp)  
  ORDER BY k.pripar, p.datspl, k.priparpo, ISNULL(f.idspolfps,0)

  EXEC KaskadaTable @predpisy, @idposezpar, @platba_c, @platba_datzap, @idadsezusr, @popis  
END
GO

CREATE PROCEDURE SparujStornoPolozky(@idtpl integer, @idposezpar integer, @idoldpol integer, @idadsezusr int, @popis nvarchar(255))
AS
BEGIN
  DECLARE @Index INT
  DECLARE @RecordCnt INT
  DECLARE @predpisy Predpisy_TableType

  DECLARE @idaasezfps integer
  DECLARE @idaapolfps integer
  DECLARE @c numeric(12,2)
  DECLARE @cpar numeric(12,2)
  DECLARE @datspl datetime

  -- zde uz stopro vime ze jde o storno pokladni polozku a musime najit jak byla parovana ta originalni
  -- pokud nasledujici select nenasel zadne zaznamy tak to znamena ze originalni polozka nebyla naparovana na predpis a proto oznacime storno polozku i originalni polozku priznakem STO
  INSERT INTO @predpisy     
  SELECT p.idaapolfps, p.idaasezfps, par.c, dbo.Parovano(p.idaapolfps) as parovano, p.datspl FROM posezpar par JOIN aapolfps p ON par.idaapolfps=p.idaapolfps WHERE par.idtpl=@idoldpol AND par.tpl='popolpod'

  SELECT @RecordCnt = COUNT(idaapolfps) FROM @predpisy
  SELECT @Index = 1

  IF (@RecordCnt > 0)
  BEGIN
    WHILE (@Index <= @RecordCnt)
    BEGIN 
      SELECT @idaapolfps=idaapolfps, @idaasezfps = idaasezfps, @c = c, @cpar = ISNULL(cpar,0), @datspl = datspl FROM @predpisy WHERE iSNo=@Index   
  	  EXEC Sparuj 'SPK',@idaasezfps, @idaapolfps, @idposezpar, @idadsezusr, @popis
      SELECT @Index = @Index + 1
    END  
  END ELSE
  BEGIN
    UPDATE posezpar SET sbv='STO', datumzmeny=GETDATE(), idadsezusr=@idadsezusr, popis=@popis WHERE idposezpar = @idposezpar
	UPDATE posezpar SET sbv='STO', datumzmeny=GETDATE(), idadsezusr=@idadsezusr, popis=@popis  WHERE idtpl = @idoldpol
  END
END
GO

CREATE PROCEDURE SparujKaskadouVratky(@idadsezusr int) AS
BEGIN
  DECLARE @idposezpar integer
  DECLARE @idaasezfps integer
  DECLARE @idoldpol integer
  DECLARE @idtpl integer
  DECLARE @platba_c numeric(12,2)
  DECLARE @platba_datzap datetime
  DECLARE @tpl varchar(10)

  

  --Vybereme pouze zaporne uhrady (vratky)
  DECLARE platby CURSOR FOR  
  SELECT idposezpar, idaasezfps, c, datzap, tpl, idtpl
  FROM posezpar  
  WHERE idaasezfps is not null and (sbv='SPA' or sbv='SPM') and akt=10 AND c<0 AND ISNULL(posezpar.tpr,'')<>'posezdav' AND posezpar.idtpr IS NULL AND idaasezfps in (SELECT DISTINCT idaasezfps FROM aapolfps WHERE aapolfps.idaasezfps=posezpar.idaasezfps AND aapolfps.akt=10 AND c<0 AND dbo.Parovano(aapolfps.idaapolfps)<>aapolfps.c)
  ORDER BY datzap
  OPEN platby
  FETCH NEXT FROM platby INTO @idposezpar, @idaasezfps, @platba_c, @platba_datzap, @tpl, @idtpl

  WHILE @@FETCH_STATUS = 0   
  BEGIN 
    -- Kaskada podle karty
	SET @idoldpol = NULL
	IF (@tpl = 'popolpod') 
	BEGIN
	  SELECT @idoldpol = idoldpol FROM popolpod WHERE idpopolpod = @idtpl
    END

    IF @idoldpol IS NULL
	BEGIN
	  EXEC Kaskada @idaasezfps, @idposezpar, @platba_c, @platba_datzap, @idadsezusr, 'Kaskádou přepárováno z karty na předpis'
    END

    FETCH NEXT FROM platby INTO @idposezpar, @idaasezfps, @platba_c, @platba_datzap, @tpl, @idtpl
  END

  CLOSE platby
  DEALLOCATE platby
END
GO


CREATE PROCEDURE SparujKaskadou(@idadsezusr int) AS
BEGIN
  DECLARE @idposezpar integer
  DECLARE @idaasezfps integer
  DECLARE @idoldpol integer
  DECLARE @idtpl integer
  DECLARE @platba_c numeric(12,2)
  DECLARE @platba_datzap datetime
  DECLARE @tpl varchar(10)

  

  DECLARE platby CURSOR FOR  
  SELECT idposezpar, idaasezfps, c, datzap, tpl, idtpl
  FROM posezpar  
  WHERE idaasezfps is not null and (sbv='SPA' or sbv='SPM') and akt=10 AND c<>0 AND ISNULL(posezpar.tpr,'')<>'posezdav' AND posezpar.idtpr IS NULL AND idaasezfps in (SELECT DISTINCT idaasezfps FROM aapolfps WHERE aapolfps.idaasezfps=posezpar.idaasezfps AND aapolfps.akt=10 AND c<>0 AND dbo.Parovano(aapolfps.idaapolfps)<>aapolfps.c)
  ORDER BY datzap
  OPEN platby
  FETCH NEXT FROM platby INTO @idposezpar, @idaasezfps, @platba_c, @platba_datzap, @tpl, @idtpl

  WHILE @@FETCH_STATUS = 0   
  BEGIN 
    -- Kaskada podle karty
	SET @idoldpol = NULL
	IF (@tpl = 'popolpod') 
	BEGIN
	  SELECT @idoldpol = idoldpol FROM popolpod WHERE idpopolpod = @idtpl
    END

    IF @idoldpol IS NOT NULL
	BEGIN
      EXEC SparujStornoPolozky @idtpl, @idposezpar, @idoldpol, @idadsezusr, 'Storno z pokladny'
	END ELSE
	BEGIN
	  EXEC Kaskada @idaasezfps, @idposezpar, @platba_c, @platba_datzap, @idadsezusr, 'Kaskádou přepárováno z karty na předpis'
    END

    FETCH NEXT FROM platby INTO @idposezpar, @idaasezfps, @platba_c, @platba_datzap, @tpl, @idtpl
  END

  CLOSE platby
  DEALLOCATE platby
END
GO

CREATE PROCEDURE SparujPodleSS(@idadsezusr int) AS
BEGIN
  DECLARE @idposezpar int
  DECLARE @ss varchar(10)
  DECLARE @vs varchar(10)
  DECLARE @platba_c numeric(12,2)
  DECLARE @platba_datzap datetime
  DECLARE sparovat CURSOR FOR  --najde pouze ty platby ktere maji alespon jeden predpis se shodnym SS
    SELECT p.idposezpar, p.vs, p.ss, p.c, p.datzap FROM posezpar p WHERE p.idaasezfps IS NULL AND p.idaapolfps IS NULL and sbv = 'NES' AND (LTrim(ISNULL(p.ss,'')) <> '' AND ISNULL(p.ss,'') <> '0000000000') AND p.akt=10 AND EXISTS (SELECT * FROM aapolfps WHERE p.ss=aapolfps.ss AND aapolfps.akt=10 AND dbo.Parovano(aapolfps.idaapolfps)<>aapolfps.c)
	--SELECT DISTINCT f.idaasezfps, p.idposezpar, p.ss, p.c, p.datzap FROM posezpar p JOIN aapolfps f ON p.ss=f.ss WHERE p.idaasezfps IS NULL AND p.idaapolfps IS NULL and sbv = 'NES' AND (LTrim(ISNULL(p.ss,'')) <> '' AND ISNULL(p.ss,'') <> '0000000000') AND p.akt=10 AND f.akt=10

  OPEN sparovat
  FETCH NEXT FROM sparovat INTO @idposezpar, @vs, @ss, @platba_c, @platba_datzap

  WHILE @@FETCH_STATUS = 0   
  BEGIN 
	EXEC KaskadaSS @vs, @ss, @idposezpar, @platba_c, @platba_datzap, @idadsezusr, 'Párováno kaskádou podle SS'
    FETCH NEXT FROM sparovat INTO @idposezpar, @vs, @ss, @platba_c, @platba_datzap
  END

  CLOSE sparovat
  DEALLOCATE sparovat
END
GO

CREATE PROCEDURE SparujNaKartuPodleVS(@idadsezusr int) AS
BEGIN
  DECLARE @idaasezfps int
  DECLARE @idposezpar int
  DECLARE @platba_c numeric(12,2)
  DECLARE @platba_datzap datetime
  DECLARE @idspolfps int
  DECLARE sparovat CURSOR FOR  
    SELECT f.idaasezfps, p.idposezpar, p.c, p.datzap, idspolfps FROM posezpar p JOIN aasezfps f ON p.vs=f.vs WHERE p.idaasezfps IS NULL AND p.idaapolfps IS NULL and sbv = 'NES' AND (LTrim(ISNULL(p.ss,''))='' OR ISNULL(p.SS,'') = '0000000000') AND (LTrim(ISNULL(p.vs,'')) <> '' AND ISNULL(p.vs,'') <> '0000000000') AND p.akt=10 AND idspolfps IS NULL AND ISNULL(spolfps,0) = 0

  OPEN sparovat
  FETCH NEXT FROM sparovat INTO @idaasezfps, @idposezpar, @platba_c, @platba_datzap, @idspolfps

  WHILE @@FETCH_STATUS = 0   
  BEGIN 
    UPDATE posezpar SET sbv='SPA', idaasezfps = @idaasezfps WHERE idposezpar = @idposezpar
	EXEC UlozDoHistorieParovani @idposezpar, 'aasezfps', @idaasezfps, @idadsezusr, 'SPA', 'Sparováno automatem na kartu podle VS bez kaskády'    
    
    FETCH NEXT FROM sparovat INTO @idaasezfps, @idposezpar, @platba_c, @platba_datzap, @idspolfps
  END

  CLOSE sparovat
  DEALLOCATE sparovat
END
GO


CREATE PROCEDURE SparujPodleVS(@idadsezusr int) AS
BEGIN
  DECLARE @idaasezfps int
  DECLARE @idposezpar int
  DECLARE @platba_c numeric(12,2)
  DECLARE @platba_datzap datetime
  DECLARE @idspolfps int
  DECLARE @tpl varchar(10)

  DECLARE sparovat CURSOR FOR  
    SELECT f.idaasezfps, p.idposezpar, p.c, p.datzap, idspolfps, p.tpl FROM posezpar p JOIN aasezfps f ON p.vs=f.vs WHERE p.idaasezfps IS NULL AND p.idaapolfps IS NULL and sbv = 'NES' AND (LTrim(ISNULL(p.vs,'')) <> '' AND ISNULL(p.vs,'') <> '0000000000') AND p.akt=10

  OPEN sparovat
  FETCH NEXT FROM sparovat INTO @idaasezfps, @idposezpar, @platba_c, @platba_datzap, @idspolfps, @tpl

  WHILE @@FETCH_STATUS = 0   
  BEGIN 
    -- chtěl jsem rozlišovat zda se bude parovat pomoci Kaskada a KaskadaNapojeni ale oboji SP funguji na zakladě předpisů, takže k napárování na kartu (SPA) nedojde
    IF @tpl = 'popolpod' 
	  EXEC Kaskada @idaasezfps, @idposezpar, @platba_c, @platba_datzap, @idadsezusr, 'Párováno kaskádou podle VS z pokladny'
    ELSE
	  EXEC KaskadaNapojeni @idaasezfps, @idposezpar, @platba_c, @platba_datzap, @idadsezusr, 'Párováno kaskádou podle VS a napojených'

    FETCH NEXT FROM sparovat INTO @idaasezfps, @idposezpar, @platba_c, @platba_datzap, @idspolfps, @tpl
  END

  CLOSE sparovat
  DEALLOCATE sparovat

  EXEC SparujNaKartuPodleVS @idadsezusr
END
GO

CREATE PROCEDURE SparujPodleSpojCisla(@idadsezusr int) AS
BEGIN
  DECLARE @idaasezfps int
  DECLARE @idposezpar int
  DECLARE @platba_c numeric(12,2)
  DECLARE @platba_datzap datetime
  DECLARE @idspolfps int
  DECLARE @tpl varchar(10)

  DECLARE sparovat CURSOR FOR  
    SELECT f.idaasezfps, p.idposezpar, p.c, p.datzap, idspolfps, p.tpl FROM posezpar p JOIN aasezfps f ON p.spojcislo=f.spojcislo WHERE p.idaasezfps IS NULL AND p.idaapolfps IS NULL and sbv = 'NES' AND p.akt=10

  OPEN sparovat
  FETCH NEXT FROM sparovat INTO @idaasezfps, @idposezpar, @platba_c, @platba_datzap, @idspolfps, @tpl

  WHILE @@FETCH_STATUS = 0   
  BEGIN 
	EXEC KaskadaNapojeni @idaasezfps, @idposezpar, @platba_c, @platba_datzap, @idadsezusr, 'Párováno kaskádou podle SpojCisla a napojených'

    FETCH NEXT FROM sparovat INTO @idaasezfps, @idposezpar, @platba_c, @platba_datzap, @idspolfps, @tpl
  END

  CLOSE sparovat
  DEALLOCATE sparovat  
END
GO



CREATE PROCEDURE SparujFakturyPodleVS(@idadsezusr int) AS
BEGIN
  DECLARE @idaasezfps int
  DECLARE @idposezpar int
  DECLARE @platba_c numeric(12,2)
  DECLARE @platba_datzap datetime
  DECLARE @vs varchar(10)
  DECLARE sparovat CURSOR FOR  
    SELECT p.idposezpar, p.c, p.datzap, p.vs FROM posezpar p WHERE p.idaasezfps IS NULL AND p.idaapolfps IS NULL and sbv = 'NES' AND (LTrim(ISNULL(p.vs,'')) <> '' AND ISNULL(p.vs,'') <> '0000000000') AND p.akt=10 ORDER BY datzap

  OPEN sparovat
  FETCH NEXT FROM sparovat INTO @idposezpar, @platba_c, @platba_datzap, @vs

  WHILE @@FETCH_STATUS = 0   
  BEGIN 
    SET @idaasezfps = 0  --pozor vynulovani musi byt jelikoz vpripade kdy select nevybere ani jednu hodnotu null id se nenastavi a bude v nem puvodni hodnota
	SELECT TOP 1 @idaasezfps = ISNULL(idaasezfps,0) FROM aasezfps f JOIN aaseztfp t ON f.tfp=t.tfp AND t.pfp in (220,230) WHERE f.akt=10 AND vs = @vs AND dbo.SaldoKarty(idaasezfps)=@platba_c ORDER BY datspl

    IF @idaasezfps>0
	BEGIN
	  UPDATE posezpar SET sbv='SBK', idaasezfps = @idaasezfps WHERE idposezpar = @idposezpar
      EXEC UlozDoHistorieParovani @idposezpar, 'aapolfps', NULL, @idadsezusr, 'SPK', 'Sparováno automatem na fakturu podle VS bez kaskády'    
    END

    FETCH NEXT FROM sparovat INTO @idposezpar, @platba_c, @platba_datzap, @vs
  END

  CLOSE sparovat
  DEALLOCATE sparovat
END
GO

/*
Zkontroluje a nastavi datum vypisu na uhradach pochazejicich ze slozenek, ale pouze tam kde datum vypisu neni vyplnen
Procedura se spouští ručně
*/
CREATE PROCEDURE ZnovuNastavDatOrg(@idadsezusr int) AS
BEGIN
  DECLARE @Index INT
  DECLARE @RecordCnt INT
  DECLARE @davky Davky_TableType

  DECLARE @idposezdav int
  DECLARE @idposezpar int
  DECLARE @platba_datorg datetime

  INSERT INTO @davky    
	SELECT p.idposezpar, d.idposezdav, p.datorg FROM posezdav d JOIN posezpar p ON d.vs=p.vs AND d.ccelk=p.c WHERE p.sbv='SBK' AND p.akt=10 

  SELECT @RecordCnt = COUNT(idposezdav) FROM @davky
  SELECT @Index = 1

  IF (@RecordCnt > 0)
  BEGIN
    WHILE (@Index <= @RecordCnt)
    BEGIN 
      SELECT @idposezdav=idposezdav, @idposezpar = idposezpar, @platba_datorg = datorg FROM @davky WHERE iSNo=@Index   

	  IF @platba_datorg IS NOT NULL
	  BEGIN
	    UPDATE posezpar SET datorg = @platba_datorg, datumzmeny=GETDATE(), idadsezusr=@idadsezusr, popis='Kontrolou doplněn datum výpisu'  WHERE datorg IS NULL AND tpl='popoldav' AND idtpl in (select idpopoldav from popoldav where idposezdav = @idposezdav) 	    
      END
      
      SELECT @Index = @Index + 1
    END
  END
END
GO

CREATE PROCEDURE SparujDavkySlozenek(@idadsezusr int) AS
BEGIN
  DECLARE @Index INT
  DECLARE @RecordCnt INT
  DECLARE @davky Davky_TableType

  DECLARE @idposezdav int
  DECLARE @idposezpar int
  DECLARE @platba_datorg datetime


  INSERT INTO @davky
    SELECT p.idposezpar, d.idposezdav, p.datorg FROM posezdav d JOIN posezpar p ON d.vs=p.vs AND d.ccelk=p.c WHERE d.sbv='NES' AND p.sbv='NES' AND p.akt=10 AND dbo.ParovanoTPR('posezdav',d.idposezdav)=0

  SELECT @RecordCnt = COUNT(idposezdav) FROM @davky
  SELECT @Index = 1

  IF (@RecordCnt > 0)
  BEGIN
    WHILE (@Index <= @RecordCnt)
    BEGIN 
      SELECT @idposezdav=idposezdav, @idposezpar = idposezpar, @platba_datorg = datorg FROM @davky WHERE iSNo=@Index   
  	  
      UPDATE posezpar SET sbv='SBK', tpr = 'posezdav', idtpr= @idposezdav, datumzmeny=GETDATE(), idadsezusr=@idadsezusr, popis='Párováno podle VS na dávku složenek'   WHERE idposezpar = @idposezpar 
	  UPDATE posezpar SET datorg = @platba_datorg, datumzmeny=GETDATE(), idadsezusr=@idadsezusr, popis='Automaticky doplněn datum výpisu při párování dávky'  WHERE tpl='popoldav' AND idtpl in (select idpopoldav from popoldav where idposezdav = @idposezdav) 
      
	  SELECT @Index = @Index + 1
	END
  END

END
GO

CREATE PROCEDURE Odparovat(@idposezpar int, @tpl varchar(10), @idtpl int, @idadsezusr int) AS
BEGIN
  DECLARE @idposezpod int

  UPDATE posezpar SET sbv='NES', idaasezfps=NULL, idaapolfps=NULL, tpr=NULL, idtpr=NULL, datumzmeny=GETDATE(), idadsezusr=@idadsezusr, popis='Odpárováno' WHERE idposezpar = @idposezpar  

  IF @tpl='popolpod' 
  BEGIN
    SELECT @idposezpod = idposezpod FROM popolpod WHERE idpopolpod = @idtpl

  END
END
GO

CREATE PROCEDURE ZapisDoHistorie(@idadsezusr int) AS
BEGIN
  DECLARE @idadsezhak int
  SELECT @idadsezhak = ISNULL(MAX(idadsezhak),0)+1 FROM adsezhak
END
GO

CREATE PROCEDURE SjednotZdvojeneUhrady(@idfps int = 0, @idadsezusr int = NULL) AS
BEGIN
DECLARE @sbv varchar(3)
DECLARE @idaasezfps int
DECLARE @idtpl int
DECLARE @tpl varchar(30)
DECLARE @datzap datetime
DECLARE @datorg datetime
DECLARE @vs varchar(10)
DECLARE @ss varchar(10)
DECLARE @akt int
DECLARE @idposezpod int
DECLARE @tpr varchar(30)
DECLARE @idtpr int
DECLARE @idaapolfps int
DECLARE @idposezpar int
DECLARE @c numeric(12,2)
DECLARE @idposezpar_novy int
DECLARE @idposezpar_zdvojeny int
DECLARE @sumaPlatebPred numeric(12,2)
DECLARE @sumaPlatebPo numeric(12,2)

BEGIN TRANSACTION;

BEGIN TRY

	SET @sumaPlatebPred=0
	SELECT @sumaPlatebPred = SUM(c) FROM posezpar WHERE akt=10 AND (@idfps IS NULL OR @idfps=idaasezfps)

	DECLARE shrink CURSOR FOR  
	SELECT MIN(idposezpar) as idposezpar, SUM(c) AS c, sbv, idaasezfps, idtpl, tpl, datzap, datorg, vs, ss, akt, idposezpod, tpr, idtpr, idaapolfps from posezpar 
	WHERE akt=10 AND (@idaasezfps IS NULL OR @idaasezfps=idaasezfps)
	GROUP by sbv, idaasezfps, idtpl, tpl, datzap, datorg, vs, ss, akt, idposezpod, tpr, idtpr, idaapolfps having count(*)>1

	OPEN shrink
	FETCH NEXT FROM shrink INTO @idposezpar, @c, @sbv, @idaasezfps, @idtpl, @tpl, @datzap, @datorg, @vs, @ss, @akt, @idposezpod, @tpr, @idtpr, @idaapolfps
	WHILE @@FETCH_STATUS = 0   
	BEGIN   
	  UPDATE posezpar SET c=@c, datumzmeny=GETDATE(), idadsezusr=@idadsezusr, popis='Změna částky při sjednocení' WHERE idposezpar=@idposezpar
        
	  UPDATE posezpar SET akt=80, datumzmeny=GETDATE(), idadsezusr=@idadsezusr, popis='Zrušení úhrady při sjednocení'
	  WHERE idposezpar<>@idposezpar AND ISNULL(sbv,'') = ISNULL(@sbv,'') AND ISNULL(idaasezfps,0) = ISNULL(@idaasezfps,0) AND ISNULL(idtpl,0) = ISNULL(@idtpl,0) AND ISNULL(tpl,'') = ISNULL(@tpl,'') AND ISNULL(datzap,'') = ISNULL(@datzap,'')
	  AND ISNULL(datorg,'') = ISNULL(@datorg,'') AND ISNULL(vs,'') = ISNULL(@vs,'') AND ISNULL(ss,'') = ISNULL(@ss,'') AND ISNULL(akt,0) = ISNULL(@akt,0) AND ISNULL(idposezpod,0) = ISNULL(@idposezpod,0) AND ISNULL(tpr,'') = ISNULL(@tpr,'') AND ISNULL(idtpr,'') = ISNULL(@idtpr,'') AND ISNULL(idaapolfps,'') = ISNULL(@idaapolfps,'')
  
	  FETCH NEXT FROM shrink INTO @idposezpar, @c, @sbv, @idaasezfps, @idtpl, @tpl, @datzap, @datorg, @vs, @ss, @akt, @idposezpod, @tpr, @idtpr, @idaapolfps
	END

	CLOSE shrink
	DEALLOCATE shrink

	SET @sumaPlatebPo = 0
	SELECT @sumaPlatebPo = SUM(c) FROM posezpar WHERE akt=10 AND (@idfps IS NULL OR @idfps=idaasezfps)

END TRY
BEGIN CATCH   
   IF @@TRANCOUNT > 0
     ROLLBACK TRANSACTION;

   EXEC ZapisChybu @idadsezusr, 'Sjednot zvdojené úhrady';
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
  IF @sumaPlatebPo <> @sumaPlatebPred
  BEGIN
    ROLLBACK TRANSACTION
    EXEC ZapisChybu @idadsezusr, 'Sjednot zvdojené úhrady - neseděla částka úhrad!';
  END ELSE
     COMMIT TRANSACTION
END

END
GO